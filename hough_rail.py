import cv2
import numpy as np
import matplotlib.pyplot as plt
import csv
import math

blur_ksize = 9
canny_low = 150
canny_high = 160
erode_kernel = np.ones((9, 9), np.uint8)
dilation_kernel = np.ones((3, 3), np.uint8)

last_frame_slope_r = 3
last_frame_slope_l = -3

def distance(x1, y1, x2, y2):
    return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5

slope_l_log = []
slope_r_log = []

slope_center_log = []


#TODO ADD moving average filter to filter out the spike of slope and bias


def process_frame(frame):
    global last_frame_slope_r
    global last_frame_slope_l

    height, width, _ = frame.shape

    # Divide the frame into left and right halves
    left_frame = frame[:, :width // 2, :]
    right_frame = frame[:, width // 2:, :]
    
    
    # Process left half
    left_gray = cv2.cvtColor(left_frame, cv2.COLOR_RGB2GRAY)
    left_histeqaul_img = cv2.equalizeHist(left_gray)
    left_erode_img = cv2.erode(left_histeqaul_img, erode_kernel, iterations=2)
    left_dilate_img = cv2.dilate(left_erode_img, dilation_kernel, iterations=1)
    left_blur_img = cv2.GaussianBlur(left_dilate_img, (blur_ksize, blur_ksize), 0)
    left_sobel_y_img = cv2.Sobel(left_blur_img, -1, 1, 0)
    ret, left_binary_img = cv2.threshold(left_sobel_y_img, 50, 255, cv2.THRESH_BINARY)
    # cv2.imshow('left_binary_img',left_binary_img)

    
    demo = None

    left_line_image = np.copy(left_frame) * 0
    rho = 1
    theta = np.pi / 180
    threshold = 10
    min_line_length = 200
    max_line_gap = 10

    left_lines = cv2.HoughLinesP(left_binary_img, rho, theta, threshold, np.array([]), min_line_length, max_line_gap)

    if left_lines is not None:
        left_lines = sorted(left_lines, key=lambda coord: distance(coord[0][0], coord[0][1], coord[0][2], coord[0][3]))
        left_lines = left_lines[-1:]

        for line in left_lines:
            x1, y1, x2, y2 = line[0]
            slope_l = (y2-y1)/(x2-x1)

            deg = np.rad2deg(math.atan2((y2-y1),(x2-x1)))
            slope_l_log.append(deg)
            

            if deg <= -50 and deg>=-90 and abs(last_frame_slope_l-slope_l)<1:
                
                cv2.putText(left_line_image, str(deg)[0:7], ((x1+x2)//2+15, (y1+y2)//2), cv2.FONT_HERSHEY_COMPLEX,1, (255, 0, 255), 2, cv2.LINE_AA)

                cv2.line(left_line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)
                last_frame_slope_l = slope_l

                # left_gray = cv2.cvtColor(left_gray, cv2.COLOR_GRAY2BGR)
                
                # left_histeqaul_img = cv2.cvtColor(left_histeqaul_img, cv2.COLOR_GRAY2BGR)
                # left_erode_img = cv2.cvtColor(left_erode_img, cv2.COLOR_GRAY2BGR)
                # left_dilate_img = cv2.cvtColor(left_dilate_img, cv2.COLOR_GRAY2BGR)
                # left_blur_img = cv2.cvtColor(left_blur_img, cv2.COLOR_GRAY2BGR)
                # left_sobel_y_img = cv2.cvtColor(left_sobel_y_img, cv2.COLOR_GRAY2BGR)
                # left_binary_img = cv2.cvtColor(left_binary_img, cv2.COLOR_GRAY2BGR)

                # temp8 = cv2.addWeighted(left_line_image, 0.6, left_frame, 0.4, 0)

                # temp0 = cv2.resize(left_frame, (360, 320))
                # temp1 = cv2.resize(left_gray, (360,320))
                # temp2 = cv2.resize(left_histeqaul_img, (360,320))
                # temp3 = cv2.resize(left_erode_img, (360,320))
                # temp4 = cv2.resize(left_dilate_img, (360,320))
                # temp5 = cv2.resize(left_blur_img, (360,320))
                # temp6 = cv2.resize(left_sobel_y_img, (360,320))
                # temp7 = cv2.resize(left_binary_img, (360,320))
                # temp8 = cv2.resize(temp8, (360,320))
                
                # demo1 = np.concatenate((temp0,temp1,temp2),axis=1)
                # demo2 = np.concatenate((temp3,temp4,temp5),axis=1)
                # demo3 = np.concatenate((temp6,temp7,temp8),axis=1)
                # demo = np.concatenate((demo1,demo2,demo3),axis=0)

                # cv2.imshow('demo', demo)

            else:
                left_lines = None


    # Process right half
    right_gray = cv2.cvtColor(right_frame, cv2.COLOR_RGB2GRAY)
    right_histeqaul_img = cv2.equalizeHist(right_gray)
    right_erode_img = cv2.erode(right_histeqaul_img, erode_kernel, iterations=3)
    right_dilate_img = cv2.dilate(right_erode_img, dilation_kernel, iterations=1)
    right_blur_img = cv2.GaussianBlur(right_dilate_img, (blur_ksize, blur_ksize), 0)
    right_sobel_y_img = cv2.Sobel(right_blur_img, -1, 1, 0)
    ret, right_binary_img = cv2.threshold(right_sobel_y_img, 30, 255, cv2.THRESH_BINARY)
    # cv2.imshow('right_binary_img',right_binary_img)

    right_line_image = np.copy(right_frame) * 0

    right_lines = cv2.HoughLinesP(right_binary_img, rho, theta, threshold, np.array([]), min_line_length, max_line_gap)

    
    if right_lines is not None:
        right_lines = sorted(right_lines, key=lambda coord: distance(coord[0][0], coord[0][1], coord[0][2], coord[0][3]))
        right_lines = right_lines[-1:]

        for line in right_lines:
            x1, y1, x2, y2 = line[0]
            slope_r = (y2-y1)/(x2-x1)

            deg = np.rad2deg(math.atan2((y2-y1), (x2-x1)))

            slope_r_log.append(deg)
            
            if deg <= 90 and deg >= 60 and abs(last_frame_slope_r - slope_r)<1:
                
                
                cv2.putText(right_line_image, str(deg)[0:6], ((x1+x2)//2+10, (y1+y2)//2), cv2.FONT_HERSHEY_COMPLEX,1, (0, 0, 255), 1, cv2.LINE_AA)
                cv2.line(right_line_image, (x1, y1), (x2, y2), (0, 0, 255), 10)
                last_frame_slope_r = slope_r
            else:
                right_lines = None


    # Combine left and right line images
    combined_line_image = np.hstack((left_line_image, right_line_image))

    if right_lines is not None and left_lines is not None:
        rx1 = right_lines[0][0][0]+640
        ry1 = right_lines[0][0][1]
        rx2 = right_lines[0][0][2]+640
        ry2 = right_lines[0][0][3]
        ar = (ry2 - ry1)/(rx2 - rx1)
        br = ry2 - ar*rx2

        lx1 = left_lines[0][0][0]
        ly1 = left_lines[0][0][1]
        lx2 = left_lines[0][0][2]
        ly2 = left_lines[0][0][3]
        al = (ly2 - ly1)/(lx2 - lx1)
        bl = ly1 - al*lx1
        # print(al, ar)
        # print(bl, br)

        cv2.line(frame, ((int((720-br)/ar)+int((720-bl)/al))//2,int(720)), ((int((0-br)/ar)+int((0-bl)/al))//2,int(0)), (255, 255, 0), 2)

        deg = np.rad2deg(math.atan2(720, (int((720-br)/ar)+int((720-bl)/al))//2 - (int((0-br)/ar)+int((0-bl)/al))//2))
        
        ac = (al+ar)/2
        bc = (bl+br)/2
        
        slope_center_log.append(deg)

        centerx = (((360-br)/ar) + (360 - bl)/al)/2
        centery = 360

        

        cv2.line(frame, (640, 0), (640, 720), (255, 255, 255), 2)
        cv2.line(frame, (0, 360), (1280, 360), (255, 255, 255), 2)
        cv2.putText(frame, '*', (int(centerx-9),int(centery+15)),cv2.FONT_HERSHEY_COMPLEX,1, (255, 255, 0), 5, cv2.LINE_AA)
        cv2.putText(frame, str(deg)[0:5], (int(centerx+9),int(centery-5)),cv2.FONT_HERSHEY_COMPLEX,1, (255, 255, 0), 1, cv2.LINE_AA)


    combo = cv2.addWeighted(frame, 0.8, combined_line_image, 1, 0)
    cv2.imshow('combo', combo)
    cv2.waitKey(1)

    return combo, demo

def process_video(input_file, output_file):
    cap = cv2.VideoCapture(input_file)
    
    # Get video properties
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    # Create a VideoWriter object to save the processed video
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_file, fourcc, fps, (frame_width, frame_height), isColor=True)
    out_demo = cv2.VideoWriter("demo.mp4", fourcc, fps, (1080, 960), isColor=True)
    
    while True:
        ret, frame = cap.read()

        if not ret:
            break

        processed_frame,demo = process_frame(frame)
        out.write(processed_frame)
        if not type(demo)==None:
            out_demo.write(demo)
    
    cap.release()
    out.release()
    out_demo.release()

    print("Video processing completed.")


# The processed video will be saved as 'output_video.mp4'.
test_video_dir = "C:/Users/Samuel/Desktop/yolov5/data/data/"
process_video(test_video_dir + '20230708_093937' + '.mp4', 'Demo/output_video.mp4')


with open('slope_l_log.csv', mode='w', newline='') as csvfile:
    # Create a CSV writer object
    csv_writer = csv.writer(csvfile)

    # Write the list to the CSV file
    csv_writer.writerow(slope_l_log)

with open('slope_r_log.csv', mode='w', newline='') as csvfile:
    # Create a CSV writer object
    csv_writer = csv.writer(csvfile)

    # Write the list to the CSV file
    csv_writer.writerow(slope_r_log)

with open('slope_center_log.csv', mode='w', newline='') as csvfile:
    # Create a CSV writer object
    csv_writer = csv.writer(csvfile)

    # Write the list to the CSV file
    csv_writer.writerow(slope_center_log)

x1 = range(1,len(slope_l_log)+1)
x2 = range(1,len(slope_r_log)+1)
x3 = range(1,len(slope_center_log)+1)

plt.plot(x1, slope_l_log)
plt.plot(x2, slope_r_log)
plt.plot(x3, slope_center_log)
plt.show()