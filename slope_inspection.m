clear;
clc;
close all;

figure;

log_dir = './log/';

center_bias = readmatrix(strcat(log_dir,'center_bias.csv'));
center_bias = center_bias';
subplot(2,1,1);
plot(center_bias);
title('center bias');
ylabel('bias');
grid on;

hold on;
center_bias_wth_ma = readmatrix(strcat(log_dir,'center_bias_wth_ma.csv'));
center_bias_wth_ma = center_bias_wth_ma';
% subplot(2,2,2);
plot(center_bias_wth_ma);
legend('wihout moving avg', 'with mvoing avg')
% title('center bias wth ma');
% ylabel('bias');
% grid on;

center_slope = readmatrix(strcat(log_dir,'center_slope.csv'));
center_slope = center_slope';
% center_slope = abs(abs(center_slope)-90);
subplot(2,1,2);
plot(center_slope);
title('center slope');
ylabel('slope (degree)');
grid on;

hold on;
center_slope_wth_ma = readmatrix(strcat(log_dir,'center_slope_wth_ma.csv'));
center_slope_wth_ma = center_slope_wth_ma';
% center_slope_wth_ma = abs(abs(center_slope_wth_ma)-90);
% subplot(2,2,4);
plot(center_slope_wth_ma);
legend('wihout moving avg', 'with mvoing avg')
% title('center slope wth ma');
% ylabel('slope (degree)');
% grid on;



