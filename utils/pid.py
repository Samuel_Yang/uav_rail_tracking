from typing import Any
import numpy as np

class PID:
    ts:np.float32
    Kp:np.float32
    Ki:np.float32
    Kd:np.float32
    e_k1:np.float32 = 0.0
    e_k2:np.float32 = 0.0
    y_k1:np.float32 = 0.0

    def __init__(self, Kp:np.float32, Ki:np.float32, Kd:np.float32,ts:np.float32):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.ts = ts

    def setPID(self, Kp:np.float32=None, Ki:np.float32=None, Kd:np.float32=None):
        if Kp is not None:
            self.Kp = Kp
        if Ki is not None:
            self.Ki = Ki
        if Kd is not None:
            self.Kd = Kd
    
    def __call__(self, e:np.float32) -> Any:
        

        self.y = self.y_k1 + self.Kp*(e - self.e_k1) + \
                            self.Ki*(e*self.ts) + \
                            self.Kd*(e-2*self.e_k1+self.e_k2)/self.ts
        # Update previous error values
        self.e_k2 = self.e_k1
        self.e_k1 = e
        
        # Update previous output value
        self.y_k1 = self.y
        return self.y
    

    