import cv2
import numpy as np
import matplotlib.pyplot as plt
import csv
import math
import time

from utils.moving_avg_filter import MovingAverageFilter

class RailExtraction():
    def __init__(self):

        # webcam attribute
        self.fps = 1 #this feature will only enabled when it is webcam mode
        self.brightness = 100
        self.contrast = 50
        self.saturation = 100

        # frame attribute
        self.width = None
        self.height = None
        self.channel = None

        # Pre-processing attribute
        self.__blur_ksize = 9
        self.__canny_low = 150
        self.__canny_high = 160

        self.__erode_kernel = np.ones((9, 9), np.uint8)
        self.__dilation_kernel = np.ones((3, 3), np.uint8)
        self.__bibary_threshold = 50

        # Hough transform attribute
        self.__rho = 1
        self.__theta = np.pi / 180
        self.__threshold = 10
        self.__min_line_length = 200
        self.__max_line_gap = 10

        # Rail attribute
        self.__left_slope_bound = (-90, -50)
        self.__right_slope_bound = (50, 90)

        self.__last_right_frame_slope = 3
        self.__last_left_frame_slope = -3

        self.left_slope_log = []
        self.right_slope_log = []
        self.center_slope_log = []


        self.center_deg_log = []
        self.center_deg_log_ma = []
        self.center_bias_log = []
        self.center_bias_log_ma = []

        self.__moving_avg_step = 10
        self.moving_avg_filter_deg = MovingAverageFilter(self.__moving_avg_step)
        self.moving_avg_filter_bias = MovingAverageFilter(self.__moving_avg_step)

        self.process_freq = None # process every n frame

        # save attribute
        self.save_enabled = None
        self.log_dir = './log/'
        self.output_video_dir = './run/'

        self.deg = None
        self.center_x = None

        self.delta_deg = None
        self.delta_bias = None

    def set_moving_avg_step_size(self, step_size: int):
        self.__moving_avg_step = step_size
        self.moving_avg_filter_bias.set_step_size(self.__moving_avg_step)
        self.moving_avg_filter_deg.set_step_size(self.__moving_avg_step)
    
    def save_enable(self, enable=True):
        """
        determine to save current frame or not
        """
        self.save_enabled = enable
    
    def calculate_slope(self, x1, y1, x2, y2):
        if x2 - x1 == 0:
            return float('inf')  # Handle vertical lines by returning infinity
        else:
            return (y2 - y1) / (x2 - x1)

    def rail_detection(self, frame: np.ndarray, frame_mode: str):
        """
        detect the rail of the frame
        """

        # Pre-Process
        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        histeqaul_img = cv2.equalizeHist(gray)
        erode_img = cv2.erode(histeqaul_img, self.__erode_kernel, iterations=2)
        dilate_img = cv2.dilate(erode_img, self.__dilation_kernel, iterations=1)
        blur_img = cv2.GaussianBlur(dilate_img, (self.__blur_ksize, self.__blur_ksize), 0)
        sobel_y_img = cv2.Sobel(blur_img, -1, 1, 0)
        ret, binary_img = cv2.threshold(sobel_y_img, self.__bibary_threshold , 255,
                                         cv2.THRESH_BINARY)
        
        # line detection base on Hough transform
        lines = cv2.HoughLinesP(binary_img, self.__rho, self.__theta, self.__threshold, 
                                np.array([]),self.__min_line_length, self.__max_line_gap)
        
        line_image = np.copy(frame) * 0
        bias = None
        slope = None
        
        if lines is not None:
            # lines = sorted(lines, key=lambda coord: math.dist((coord[0][0], coord[0][1]), 
            #                                                             (coord[0][2], coord[0][3])))
            # lines = lines[-1:]
            lines = max(lines, key=lambda coord: math.dist((coord[0][0], coord[0][1]), (coord[0][2], coord[0][3])))
            # print(lines)

            
                                                 

            for line in lines:
                x1, y1, x2, y2 = line
                slope = self.calculate_slope(x1, y1, x2, y2)
                
                deg = np.rad2deg(math.atan2((y2-y1),(x2-x1)))
                

                if frame_mode=='left':
                    bias = y1 - slope*x1
                    if deg > self.__left_slope_bound[0] and deg < self.__left_slope_bound[1] \
                                                    and abs(self.__last_left_frame_slope - slope)<1:
                        self.left_slope_log.append(slope)
                        
                        cv2.putText(line_image, str(deg)[0:6], ((x1+x2)//2+15, (y1+y2)//2), \
                                        cv2.FONT_HERSHEY_COMPLEX,1, (255, 0, 255), 2, cv2.LINE_AA)

                        cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)
                        self.__last_left_frame_slope = slope

                else:
                    bias = y1 - slope*(x1+self.width/2)
                    if deg > self.__right_slope_bound[0] and slope < self.__right_slope_bound[1] \
                                                    and abs(self.__last_right_frame_slope - slope)<1:
                        self.right_slope_log.append(slope)
                        

                        cv2.putText(line_image, str(deg)[0:6], ((x1+x2)//2+15, (y1+y2)//2), \
                                        cv2.FONT_HERSHEY_COMPLEX,1, (255, 0, 255), 2, cv2.LINE_AA)

                        cv2.line(line_image, (x1, y1), (x2, y2), (0, 0, 255), 10)
                        self.__last_right_frame_slope = slope

        else:
            print('no line in the frame')

        frame = cv2.addWeighted(frame, 0.5, line_image, 0.5, 0)

        cv2.imshow(frame_mode+'frame', frame)

        return slope, bias, lines, frame
    
    def center_line_extraction(self,frame, left_slope,left_bias,right_slope, right_bias):
        """
        detect the center of two rails
        """ 

        center_slope = self.height / (((self.height-right_bias)/right_slope+(self.height-left_bias)/left_slope)/2 - 
                                      ((0-right_bias)/right_slope+(0-left_bias/left_slope))/2)
        center_bias = (right_bias + left_bias) / 2

        self.center_slope_log.append(center_slope)


        center_x = (((self.height/2-right_bias)/right_slope) + (self.height/2 - left_bias)/left_slope)/2
        center_y = self.height/2

        x1 = int(((self.height-right_bias)/right_slope+(self.height-left_bias)/left_slope)/2)
        y1 = int(self.height)
        x2 = int(((0-right_bias)/right_slope+(0-left_bias)/left_slope)/2)
        y2 = int(0)
        
        
        cv2.line(frame, (x1, y1),(x2, y2), (255, 255, 0), 2)
        
        # if abs(center_slope)<1e-5:
        #     cv2.line(frame, ( int(center_bias) ,int(self.height)), (int(center_bias),0), (255, 255, 0), 2)
        # else:
        #     cv2.line(frame, ( int((self.height - center_bias)/center_slope) ,int(self.height)), (int(-center_bias/center_slope),0), (255, 255, 0), 2)


        deg = np.arctan(center_slope) * 180 / np.pi
        
        self.delta_deg = (90 - np.abs(deg))
        self.delta_bias = center_x - self.width/2

        self.delta_deg = self.moving_avg_filter_deg.update(self.delta_deg)
        self.delta_bias = self.moving_avg_filter_bias.update(self.delta_bias)
        

        # draw center line
        cv2.line(frame, (self.width//2, 0), (self.width//2, self.height), (255, 255, 255), 1)
        cv2.line(frame, (0, self.height//2), (self.width, self.height//2), (255, 255, 255), 1)


        cv2.line(frame, (int(center_x),int(center_y)), (self.width//2, self.height//2), (255, 255, 255), 5)
        cv2.putText(frame, str(self.delta_bias)[0:5], (int(self.width//2),int(self.height//2+25)),cv2.FONT_HERSHEY_COMPLEX,1, (255, 255, 255), 1, cv2.LINE_AA)

        cv2.putText(frame, '*', (int(center_x-9),int(center_y+15)),cv2.FONT_HERSHEY_COMPLEX,1, (255, 255, 0), 5, cv2.LINE_AA)
        cv2.putText(frame, str(self.delta_deg)[0:4], (int(center_x+9),int(center_y-5)),cv2.FONT_HERSHEY_COMPLEX,1, (255, 255, 0), 1, cv2.LINE_AA)



        # self.center_deg_log.append(deg)
        # self.center_bias_log.append(center_x)
        

        return frame, deg, center_x


    def rail_extraction(self, frame: np.ndarray):
        """
        detect the rail of the frame
        """
        self.height, self.width, self.channel = frame.shape

        # Divide the frame into left and right halves
        left_frame = frame[:, :self.width // 2, :]
        right_frame = frame[:, self.width // 2:, :]

        left_slope, left_bias, left_line, left_frame = self.rail_detection(left_frame, 'left')
        right_slope, right_bias, right_line, right_frame = self.rail_detection(right_frame, 'right')
        frame = np.hstack((left_frame, right_frame))

        print(left_bias, left_slope, right_bias, right_slope)


        if left_line is not None and right_line is not None:
            frame, self.deg, self.center_x = self.center_line_extraction(frame, left_slope, left_bias,right_slope, right_bias)
            
            self.center_deg_log.append(self.delta_deg)
            self.center_bias_log.append(self.delta_bias)


        return frame


    def cam_initialize(self, cap):
        # Set video resolution (width, height)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, self.width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self.height)

        # Set frame rate (fps)
        cap.set(cv2.CAP_PROP_FPS, self.fps)

        # Set brightness (0 to 255, where 0 is the darkest and 255 is the brightest)
        cap.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)

        # Set contrast (0 to 255, where 0 is the lowest contrast and 255 is the highest)
        cap.set(cv2.CAP_PROP_CONTRAST, self.contrast)

        # Set saturation (0 to 255, where 0 is the least saturated and 255 is the most saturated)
        cap.set(cv2.CAP_PROP_SATURATION, self.saturation)

        return cap


    def process_video(self,input_file, output_file:str()=None):
        if input_file is None:
            # Process webcam input
            cap = cv2.VideoCapture(0)
            cap = self.cam_initialize(cap)

        else:
            # Process video file
            cap = cv2.VideoCapture(input_file)

        # Get video properties
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        # Create a VideoWriter object to save the processed video
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')
        if output_file is not None:
            out = cv2.VideoWriter(self.output_video_dir + output_file, fourcc, fps, (frame_width, frame_height), isColor=True)

        

        while True:
            ret, frame = cap.read()
            
            if not ret:
                break

            start_time = time.time()

            processed_frame = self.rail_extraction(frame)

            cv2.imshow('result', processed_frame)
            cv2.waitKey(1)

            # delta_deg = self.moving_avg_filter_deg.update(self.delta_deg)
            self.center_deg_log_ma.append(self.delta_deg)
            
            # delta_bias = self.moving_avg_filter_bias.update(self.delta_bias)
            self.center_bias_log_ma.append(self.delta_bias)

            end_time = time.time()
            processing_time = end_time - start_time
            # print(processing_time)


            if self.save_enabled:
                out.write(processed_frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        
        cap.release()
        if output_file is not None:
            out.release()

        print("processing completed.")

    def save_log(self):
        with open(self.log_dir + 'center_slope.csv', mode='w', newline='') as csvfile:
            # Create a CSV writer object
            csv_writer = csv.writer(csvfile)

            # Write the list to the CSV file
            csv_writer.writerow(self.center_deg_log)

        with open(self.log_dir + 'center_slope_wth_ma.csv', mode='w', newline='') as csvfile:
            # Create a CSV writer object
            csv_writer = csv.writer(csvfile)

            # Write the list to the CSV file
            csv_writer.writerow(self.center_deg_log_ma)

        with open(self.log_dir + 'center_bias.csv', mode='w', newline='') as csvfile:
            # Create a CSV writer object
            csv_writer = csv.writer(csvfile)

            # Write the list to the CSV file
            csv_writer.writerow(self.center_bias_log)

        with open(self.log_dir + 'center_bias_wth_ma.csv', mode='w', newline='') as csvfile:
            # Create a CSV writer object
            csv_writer = csv.writer(csvfile)

            # Write the list to the CSV file
            csv_writer.writerow(self.center_bias_log_ma)




        




        



    

    


    

