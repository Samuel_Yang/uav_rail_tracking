import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import cv2

blur_ksize = 9
canny_low = 150
canny_high = 160
erode_kernel = np.ones((9, 9), np.uint8)
dilation_kernel = np.ones((3, 3), np.uint8)

def distance(x1, y1, x2, y2):
    return ((x2 - x1) ** 2 + (y2 - y1) ** 2) ** 0.5

def process_frame(frame):

    height, width, _ = frame.shape
    cv2.imshow('frame', frame)

    # Divide the frame into left and right halves
    left_frame = frame[:, :width // 2, :]
    right_frame = frame[:, width // 2:, :]
    cv2.imshow('right_frame', right_frame)
    
    # Process left half
    left_gray = cv2.cvtColor(left_frame, cv2.COLOR_RGB2GRAY)
    # left_histeqaul_img = cv2.equalizeHist(left_gray)
    left_erode_img = cv2.erode(left_gray, erode_kernel, iterations=1)
    left_dilate_img = cv2.dilate(left_erode_img, dilation_kernel, iterations=1)
    left_blur_img = cv2.GaussianBlur(left_dilate_img, (blur_ksize, blur_ksize), 0)
    left_sobel_y_img = cv2.Sobel(left_blur_img, -1, 1, 0)
    ret, left_binary_img = cv2.threshold(left_sobel_y_img, 50, 255, cv2.THRESH_BINARY)


    left_line_image = np.copy(left_frame) * 0
    rho = 1
    theta = np.pi / 180
    threshold = 200
    min_line_length = 20
    max_line_gap = 50

    left_lines = cv2.HoughLinesP(left_binary_img, rho, theta, threshold, np.array([]), min_line_length, max_line_gap)

    if left_lines is not None:
        left_lines = sorted(left_lines, key=lambda coord: distance(coord[0][0], coord[0][1], coord[0][2], coord[0][3]))
        left_lines = left_lines[-1:]

        for line in left_lines:
            x1, y1, x2, y2 = line[0]
            # print(f'{x1} {y1} {x2} {y2}')
            if (y1-y2)/(x1-x2) <= -2:
                cv2.putText(left_line_image, str((y1-y2)/(x1-x2)), ((x1+x2)//2, (y1+y2)//2),
                             cv2.FONT_HERSHEY_COMPLEX,1, (255, 0, 255), 2, cv2.LINE_AA)

                cv2.line(left_line_image, (x1, y1), (x2, y2), (255, 0, 0), 10)

    # Process right half
    right_gray = cv2.cvtColor(right_frame, cv2.COLOR_RGB2GRAY)
    cv2.imshow('right_gray', right_gray)
    # right_histeqaul_img = cv2.equalizeHist(right_gray)
    # cv2.imshow('right_histeqaul_img', right_histeqaul_img)
    right_erode_img = cv2.erode(right_gray, erode_kernel, iterations=1)
    cv2.imshow('right_erode_img', right_erode_img)
    right_dilate_img = cv2.dilate(right_erode_img, dilation_kernel, iterations=1)
    cv2.imshow('right_dilate_img', right_dilate_img)
    right_blur_img = cv2.GaussianBlur(right_dilate_img, (blur_ksize, blur_ksize), 0)
    cv2.imshow('right_blur_img', right_blur_img)
    right_sobel_y_img = cv2.Sobel(right_blur_img, -1, 1, 0)
    cv2.imshow('right_sobel_y_img', right_sobel_y_img)
    ret, right_binary_img = cv2.threshold(right_sobel_y_img, 50, 255, cv2.THRESH_BINARY)
    cv2.imshow('right_binary_img', right_binary_img)

    right_line_image = np.copy(right_frame) * 0

    right_lines = cv2.HoughLinesP(right_binary_img, rho, theta, threshold, np.array([]), min_line_length, max_line_gap)

    if right_lines is not None:
        right_lines = sorted(right_lines, key=lambda coord: distance(coord[0][0], coord[0][1], coord[0][2], coord[0][3]))
        right_lines = right_lines[-1:]

        binary_img = np.concatenate((left_binary_img, right_binary_img),axis=1)
        cv2.imshow('binary_img', binary_img)

        for line in right_lines:
            x1, y1, x2, y2 = line[0]
            if (y1-y2)/(x1-x2) >= 1.5:
                
                cv2.putText(right_line_image, str((y1-y2)/(x1-x2)), ((x1+x2)//2, (y1+y2)//2), cv2.FONT_HERSHEY_COMPLEX,1, (0, 0, 255), 1, cv2.LINE_AA)
                cv2.line(right_line_image, (x1, y1), (x2, y2), (0, 0, 255), 10)
    cv2.imshow('right_line_image', right_line_image)
    right_line_image2 = cv2.addWeighted(right_line_image, 0.8, right_frame, 1, 0)
    cv2.imshow('right_line_image', right_line_image)

    # Combine left and right line images
    combined_line_image = np.hstack((left_line_image, right_line_image))



    # cv2.putText(frame, 'X', (640,360),cv2.FONT_HERSHEY_COMPLEX,1, (255, 255, 255), 10, cv2.LINE_AA)

    if right_lines is not None and left_lines is not None:
        rx1 = right_lines[0][0][0]+640
        ry1 = right_lines[0][0][1]
        rx2 = right_lines[0][0][2]+640
        ry2 = right_lines[0][0][3]
        ar = (ry2 - ry1)/(rx2 - rx1)
        br = ry2 - ar*rx2

        lx1 = left_lines[0][0][0]
        ly1 = left_lines[0][0][1]
        lx2 = left_lines[0][0][2]
        ly2 = left_lines[0][0][3]
        al = (ly2 - ly1)/(lx2 - lx1)
        bl = ly1 - al*lx1

        cv2.line(frame, ((int((720-br)/ar)+int((720-bl)/al))//2,int(720)), ((int((0-br)/ar)+int((0-bl)/al))//2,int(0)), (255, 255, 0), 2)
        ac = (al+ar)/2
        bc = (bl+br)/2

        centerx = (((360-br)/ar) + (360 - bl)/al)/2
        centery = 360
        

        cv2.line(frame, (640, 0), (640, 720), (255, 255, 255), 2)
        cv2.line(frame, (0, 360), (1280, 360), (255, 255, 255), 2)
        cv2.putText(frame, '*', (int(centerx-9),int(centery+15)),cv2.FONT_HERSHEY_COMPLEX,1, (255, 255, 0), 5, cv2.LINE_AA)

    combo = cv2.addWeighted(frame, 0.8, combined_line_image, 1, 0)
    cv2.imshow('combo', combo)
    cv2.waitKey(10)

    return combo

def process_image(input_file, output_file):
    frame = cv2.imread(input_file)
    
    processed_frame = process_frame(frame)
    while True:
        key = cv2.waitKey(0) & 0xFF
        if key == ord('q'):  # 'q' key pressed, exit the loop and close the window
            break
        elif key == 27:  # 'Esc' key pressed, exit the loop and close the window
            break

    # cv2.imwrite(output_file, processed_frame)

    print("Image processing completed.")

process_image('./Image/2.jpg', 'output_video222.mp4')