#%%
from rail_extraction import RailExtraction
from utils.moving_avg_filter import MovingAverageFilter


rail_extract = RailExtraction()

rail_extract.set_moving_avg_step_size(step_size=10)
rail_extract.save_enable(False)
#%%
test_video_dir = "../rail_data/"


# rail_extract.process_video(test_video_dir + '20230708_093937' + '.mp4')

rail_extract.process_video(0)

#%%

# rail_extract.save_log()
# %%
